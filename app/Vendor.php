<?php

namespace App;

class Vendor extends Model
{
    public function stocks()
    {
        return $this->hasMany('App\Stock');
    }

    public function bills()
    {
        return $this->hasManyThrough('App\Bill', 'App\Stock');
    }
}
