<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OtherPay;
use App\Cash;
use Carbon\Carbon;
use Validator;

class OtherPayController extends Controller
{
    public function index(Request $request)
    {
        $op = OtherPay::all();

        if ($request->start) {
            $op = OtherPay::whereBetween('created_at', [$request->start, $request->end])->get();
        }

        return response()->json($op);
    }

    public function store(Request $request){
        $messages = [
            'required' => 'Mohon masukan data.',
        ];
        $validator = Validator::make($request->all(), [
            'ket' => 'required',
            'price' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $trx = OtherPay::create([
            'ket' => $request->ket,
            'price' => $request->price,
            'created_at' => $request->date,
            'cash_id' => $request->cash_id
        ]);

        $cash = Cash::find($request->cash_id);
        $cash->update(['ammount' => $cash->ammount - $request->price]);

        return response(null, 202);
    }

    public function show($id){
        $op = OtherPay::find($id);

        return response()->json($op);
    }

    public function update($id, Request $request){
        $op = OtherPay::find($id);

        $cash = Cash::find($op->cash_id);
        $cash->update(['ammount' => $cash->ammount + $op->price]);

        $messages = [
            'required' => 'Mohon masukan data.',
        ];
        $validator = Validator::make($request->all(), [
            'ket' => 'required',
            'price' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $op->update([
            'ket' => $request->ket,
            'price' => $request->price,
            'created_at' => $request->date
        ]);

        $cash = Cash::find($request->cash_id);
        $cash->update(['ammount' => $cash->ammount - $request->price]);

        return response(null, 202);
    }

    public function destroy($id){

        $op = OtherPay::find($id);
        $cash = Cash::find($op->cash_id);
        $cash->update(['ammount' => $cash->ammount + $op->price]);
        $op->delete();

        return response(null, 202);
    }
}
