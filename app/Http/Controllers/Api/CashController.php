<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cash;

class CashController extends Controller
{
    public function index(){
        return response()->json(Cash::all());
    }

    public function show($id){
        return response()->json(Cash::find($id));
    }

    public function store(Request $request){
        if (is_null($request->name)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['name' => ['Mohon diisi.']]
            ], 422);
        }
        Cash::create([
            'name' => $request->name,
            'ammount' => $request->ammount
        ]);
        return response(null, 202);
    }

    public function update(Request $request){
        if (is_null($request->name)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['name' => ['Mohon diisi.']]
            ], 422);
        }
        Cash::find($request->id)->update($request->all());
        return response(null, 202);
    }
}
