<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;
use App\StockAdj;

class StockController extends Controller
{
    public function index()
    {
        $stocks = Stock::all();
        $stocks = DB::table('stocks')
            ->leftJoin('vendors', 'stocks.vendor_id', '=', 'vendors.id')
            ->select('stocks.id as id', 'stocks.name as name', 'qty', 'vendors.name as vendor')
            ->orderBy('name')
            ->get();
        return response()->json($stocks);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        if (is_null($request->vendor_id)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['vendor' => ['Silahkan pilih supplier produk.']]
            ], 422);
        }
        Stock::create([
            'name' => $request->name,
            'vendor_id' => $request->vendor_id
        ]);
        return response(null, 202);
    }

    public function show($id)
    {
        $stock = Stock::with('vendor')->find($id);
        $vendor = $stock->vendor->name;
        unset($stock['vendor']);
        $stock['vendor'] = $vendor;
        return response()->json($stock);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $stock = Stock::find($request->id);
        $stock->update(['name' => $request->name]);
        return response(null, 202);
    }

    public function destroy($id)
    {
        $invoices = DB::table('invoice_items')
                    ->where('stock_id', $id)
                    ->get()
                    ->groupBy('invoice_id')
                    ->keys();
        if ($invoices->isNotEmpty()) {
            return response()->json('Produk tidak bisa dihapus karena digunakan dalam penjualan no ' . $invoices->implode(', '), 422);
        }

        $bills = DB::table('bill_items')
                    ->where('stock_id', $id)
                    ->get()
                    ->groupBy('bill_id')
                    ->keys();
        if ($bills->isNotEmpty()) {
            return response()->json('Produk tidak bisa dihapus karena digunakan dalam penjualan no ' . $bills->implode(', '), 422);
        }

        $stock = Stock::find($id)->delete();
        return response(null, 202);
    }

    public function indexAdj($id)
    {
        $adjs = StockAdj::where('stock_id', '=', $id)->get();
        return response()->json($adjs);
    }


    public function storeAdj(Request $request)
    {
        $stock = Stock::find($request->stock_id);
        StockAdj::create([
            'stock_id' => $request->stock_id,
            'qty' => $request->qty,
            'pqty' => $stock->qty
        ]);
    
        $stock->update(['qty' => $request->qty]);
        return response(null, 202);
    }
}
