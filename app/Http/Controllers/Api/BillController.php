<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;
use App\BillItem;
use App\BillPay;
use App\Stock;
use Carbon\Carbon;
use App\Cash;

class BillController extends Controller
{
    public function index(Request $request)
    {
        $query = Bill::with('vendor', 'items', 'payments');
        if ($request->start) {
            $query = Bill::whereBetween('created_at', [$request->start, $request->end])->with('vendor', 'items', 'payments');
        }
        $bill = $query->get();

        $bill->each(function($item){
            $vendor = $item['vendor']['name'];
            unset($item->vendor);
            $item->vendor = $vendor;
            $item->price = $item->items->reduce(function ($carry, $value) {
                return $carry + ($value->qty * $value->rate);
            }, 0);
            $item->payment = $item->payments->reduce(function ($carry, $value) {
                return $carry + $value->payment;
            }, 0);
            $item->due = $item->price - $item->payment;

        });
        return response()->json($bill);
    }

    public function store(Request $request)
    {
       
        if (!array_key_exists('vendor', $request[0])) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['vendor' => ['The vendor field is required.']]
            ], 422);
        }

        if (is_null($request[0]['vendor'])) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => ['vendor' => ['The vendor field is required.']]
            ], 422);
        }

        $bill = Bill::create([
            'vendor_id' => $request[0]['vendor'],
            'created_at' => $request[0]['date']
        ]);

        $items = [];
        if ($request[1]) {
            foreach ($request[1] as $key => $item) {
                if (!is_null($item['stock_id'])) {
                    $items[] = new BillItem(['stock_id' => $item['stock_id'], 'scale' => $item['scale'], 'qty' => $item['qty'], 'rate' => $item['rate'], 'ket' => $item['ket'], 'status' => filter_var($item['status'], FILTER_VALIDATE_BOOLEAN)]);

                }
            }
        }

        $bill->items()->saveMany($items);

        foreach ($bill->items()->get() as $item) {
            if ($item->status) {
                $stock = Stock::find($item->stock_id);
                $stock->update(['qty' => $stock->qty + $item->qty]);
            }
        }

        $payments = [];
        if ($request[2]) {
            foreach ($request[2] as $key => $item) {
                if (!is_null($item['payment'])) {
                    if ($item['payment'] != 0) {
                        $payments[] = new BillPay(['payment' => $item['payment'], 'cash_id' => $item['cash_id'], 'created_at' => $item['created_at']]);
                    }
                }
            }
        }
        $bill->payments()->saveMany($payments);
        foreach ($bill->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount - $item->payment]);
        }
        
        return response(null, 202);
    }

    public function show($id)
    {
        $bill = Bill::with('vendor', 'items', 'payments')->find($id);
        return response()->json($bill);
    }

    public function update(Request $request)
    {

        $bill = Bill::find($request[0]['id']);
        $bill->update([
            'created_at' => $request[0]['date']
        ]);

        foreach ($bill->items()->get() as $item) {
            if ($item->status) {
                $stock = Stock::find($item->stock_id);
                $stock->update(['qty' => $stock->qty - $item->qty]);
            }
        }

        BillItem::where('bill_id', '=', $request[0]['id'])->delete();

        $items = [];
        if ($request[1]) {
            foreach ($request[1] as $key => $item) {
                if (!is_null($item['stock_id'])) {
                    $items[] = new BillItem(['stock_id' => $item['stock_id'], 'scale' => $item['scale'], 'qty' => $item['qty'], 'rate' => $item['rate'], 'ket' => $item['ket'], 'status' => filter_var($item['status'], FILTER_VALIDATE_BOOLEAN)]);
                }
            }
        }

        $bill->items()->saveMany($items);

        foreach ($bill->items()->get() as $item) {
            if ($item->status) {
                $stock = Stock::find($item->stock_id);
                $stock->update(['qty' => $stock->qty + $item->qty]);
            }
        }

        foreach ($bill->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount + $item->payment]);
        }
        BillPay::where('bill_id', '=', $request[0]['id'])->delete();
        $payments = [];
        if ($request[2]) {
            foreach ($request[2] as $key => $item) {
                if (!is_null($item['payment'])) {
                    if ($item['payment'] != 0) {
                        $payments[] = new BillPay(['payment' => $item['payment'], 'cash_id' => $item['cash_id'], 'created_at' => $item['created_at']]);
                    }
                }
            }
        }
        $bill->payments()->saveMany($payments);
        foreach ($bill->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount - $item->payment]);
        }

        return response(null, 202);
    }

    public function destroy($id)
    {
        $bill = Bill::find($id);
        foreach ($bill->items()->get() as $item) {
            $stock = Stock::find($item->stock_id);
            $stock->update(['qty' => $stock->qty - $item->qty]);
        }
        
        foreach ($bill->payments()->get() as $item) {
            $cash = Cash::find($item->cash_id);
            $cash->update(['ammount' => $cash->ammount + $item->payment]);
        }

        BillItem::where('bill_id', '=', $id)->delete();
        BillPay::where('bill_id', '=', $id)->delete();
        $bill->delete();
        return response(null, 202);
    }

    public function storePayment(Request $request)
    {

        $billPay = BillPay::create([
            'payment' => $request->payment,
            'bill_id' => $request->bill_id,
            'created_at' => Carbon::parse($request->created_at)->setTimezone('Asia/Jakarta')
        ]);
        return response(null, 202);
    }

    public function destroyPayment($id)
    {
        BillPay::find($id)->delete();
        return response(null, 202);
    }
}
