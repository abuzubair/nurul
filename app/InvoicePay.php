<?php

namespace App;

class InvoicePay extends Model
{
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}
