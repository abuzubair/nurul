<?php

namespace App;

class Invoice extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function items()
    {
        return $this->hasMany('App\InvoiceItem');
    }

    public function payments()
    {
        return $this->hasMany('App\InvoicePay');
    }
}
