<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/s', function () {
//     return view('dashboard');
// });

// Route::get('/', function () {
//     return view('index');
// });
use Illuminate\Support\Facades\DB;

Route::get('/delete', function(){
    DB::table('stocks')
        ->whereNotNull('deleted_at')
        ->delete();
    DB::table('bill_items')
        ->whereNotNull('deleted_at')
        ->delete();
    DB::table('bill_pays')
        ->whereNotNull('deleted_at')
        ->delete();

        return('d');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/{vue_capture?}', function () {
    if (Auth::check()) {
        return view('index');
    }
    return view('auth.signin');
})->where('vue_capture', '[\/\w\.-]*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
