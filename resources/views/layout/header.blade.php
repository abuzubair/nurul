<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
  <div class="m-container m-container--fluid m-container--full-height">
    <div class="m-stack m-stack--ver m-stack--desktop">

      <!-- BEGIN: Brand -->
      <div class="m-stack__item m-brand  m-brand--skin-light ">
        <div class="m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="/" class="m-brand__logo-wrapper">
              <img alt="" src="/images/logo-2.png">
            </a>
          </div>
          <div class="m-stack__item m-stack__item--middle m-brand__tools">

            <!-- BEGIN: Left Aside Minimize Toggle -->
            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
              <span></span>
            </a>
            <!-- END -->

            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>
            <!-- END -->


          </div>
        </div>
      </div>
      <!-- END: Brand -->

      <!-- BEGIN: Topbar -->
      <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

        <div id="m_header_menu" class="m-topbar m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light" style="float:right">
          <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
              <a href="#" class="m-menu__link m-menu__toggle">
                {{--  <span class="m-menu__link-text">{{displayName(auth()->user())}}</span>  --}}
              </a>
            </li>
          </ul>
        </div>

        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">

          <div class="m-stack__item m-topbar__nav-wrapper">

          </div>
        </div>
      </div>
      <!-- END: Topbar -->
    </div>
  </div>
</header>