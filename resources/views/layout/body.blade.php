@extends('layout.html')

@section('body')

	<body class="m-page--fluid m--skin- m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin::Page loader -->
		{{--  <div class="m-page-loader">
			<div class="m-spinner m-spinner--brand"></div>
		</div>  --}}
		<!-- end::Page Loader -->

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- begin: Header -->
			@include('layout.header')
			<!-- END: Header -->		

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				@include('layout.leftaside')
				<!-- END: Left Aside -->
				@yield('content')

			</div>
			<!-- end:: Body -->


			<!-- begin::Footer -->
			@include('layout.footer')
			<!-- end::Footer -->
			
		</div>
		<!-- end:: Page -->

		<!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->

		<!--begin::Base Scripts -->
		<script src="{{ mix('/js/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ mix('/js/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>

		<!--end::Base Scripts -->
		@yield('contentscript')
		
		<!-- end::Page Loader -->
	</body>
	<!-- end::Body -->
@endsection