<?php

use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Vendor::class, 3)
            ->create()
            ->each(function($vendor) {
                $vendor->stocks()->saveMany(factory(App\Stock::class,rand(1, 2))->make());
            });
    }
}
